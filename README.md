# README #

Sample project to control your budget.

To run on a local machine:

1. clone this repository;

2. build project (./gradlew clean build)

3. run Spring Boot app in IDE or run through 'java -jar <path_to_war>'

4. go to http://localhost:8080