let path = require('path');
let express = require('express');
let httpProxy = require("http-proxy");
let webpack = require('webpack');
let config = require('./config/dev');

let publicPath = config.output.publicPath;

const DEV_PORT = 4000;
const REST_HOST = 'localhost';
const REST_PORT = 8080;

let app = express();
let apiProxy = httpProxy.createProxyServer();
let compiler = webpack(config);

app.use(require('webpack-dev-middleware-webpack-2')(compiler, {
    noInfo: true,
    publicPath: publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

// Proxy api requests
app.use(publicPath + "api/*", function (req, res) {
    req.url = req.originalUrl;
    apiProxy.web(req, res, {
        target: {
            port: REST_PORT,
            host: REST_HOST
        }
    });
});

app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, 'src/index.html'));
});

app.listen(DEV_PORT, function (err) {
    if (err) {
        console.log(err);
        return;
    }

    console.log('FrontEnd started at http://localhost:' + DEV_PORT);
    console.log('BackEnd required at http://' + REST_HOST + ':' + REST_PORT);
});
