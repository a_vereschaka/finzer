import * as React from "react";
import {Button, Dropdown, Input} from "semantic-ui-react";
import AppUtils from "../utils/AppUtils";

export default class TransactionAddPanel extends React.Component {

    transTypes = [
        {
            key: 'INCOME',
            text: 'INCOME',
            value: 'INCOME'
        },
        {
            key: 'OUTCOME',
            text: 'OUTCOME',
            value: 'OUTCOME'
        }
    ];

    constructor(props) {
        super(props);
        this.state = {
            amount: 0,
            desc: "",
            type: "",
            category: ""
        }
    }

    handleChange = (e, {name, value}) => this.setState({[name]: value})

    handleSubmit = () => {
        AppUtils.httpPost("transaction/create?"
            + (this.getParam("amount") + "&")
            + (this.getParam("desc") + "&")
            + (this.getParam("type") + "&")
            + (this.getParam("category") + "&")
            + ("timestamp="+ Date.now() + "&")
        );
    };

    getParam = (name) => {
        return name + "=" + this.state[name];
    };

    render() {
        return <div className="transactionsAddPanel">
            <Input name="amount" fluid placeholder="Enter amount" onChange={this.handleChange}/>
            <CategoryDropdown name="category" onChange={this.handleChange}/>
            <Input name="desc" fluid placeholder="Enter description (not required)" onChange={this.handleChange}/>
            <Dropdown
                name="type"
                options={this.transTypes}
                placeholder='Choose Type'
                selection
                fluid
                onChange={this.handleChange}
            />
            <Dropdown
                name="currency"
                options={[]}
                placeholder='Choose currency (WILL BE AVAILABLE SOON)'
                selection
                disabled
                fluid
                onChange={this.handleChange}
            />

            <Button fluid circular animated='fade' onClick={this.handleSubmit}>
                <Button.Content visible>
                    +
                </Button.Content>
                <Button.Content hidden>
                    Push the transactions
                </Button.Content>
            </Button>
        </div>
    }
}

class CategoryDropdown extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            options: [],
            currentValue: ''
        }
    }

    handleAddition = (e, {value}) => {
        this.setState({
            options: [{text: value, value}, ...this.state.options],
        })
    };

    handleChange = (e, {name, value}) => {
        this.setState({currentValue: value});
        this.props.onChange(e, {name, value})
    };

    componentWillMount() {
        AppUtils.httpGet('category/categories').then((response) => {
            const mappedOptions = response.data.map((cat) => {
                return {
                    key: cat.value,
                    text: cat.text,
                    value: cat.value
                }
            });
            this.setState({
                options: mappedOptions
            })
        });
    }

    render() {
        const {currentValue, options} = this.state;


        return (
            <Dropdown
                name="category"
                options={options}
                placeholder='Choose Category'
                search
                selection
                fluid
                allowAdditions
                value={currentValue}
                onAddItem={this.handleAddition}
                onChange={this.handleChange}
            />
        )
    }
}


