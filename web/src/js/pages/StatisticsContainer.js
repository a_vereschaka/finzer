import * as React from "react";
import {Segment} from "semantic-ui-react";
import AppUtils from "../utils/AppUtils";
import {
    CartesianGrid,
    Legend,
    Line,
    LineChart,
    PolarAngleAxis,
    PolarGrid,
    PolarRadiusAxis,
    Radar,
    RadarChart,
    RadialBar,
    RadialBarChart,
    Tooltip,
    XAxis,
    YAxis
} from "recharts";


export default class StatisticsContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            transactions: []
        }
    }

    componentWillMount() {

        const findInArr = (arr, name) => {
            for (let item of arr) {
                if (item.name === name) {
                    return item;
                }
            }
            return null;
        };

        const getStructuredData = (response) => {
            let transactionsDraft = [];
            for (let t of response.data) {
                if (t.type === "INCOME"){
                    continue;
                }
                let item = findInArr(transactionsDraft, t.category);
                if (item === null) {
                    item = {
                        name: t.category,
                        category: t.category,
                        value: 0
                    };
                    transactionsDraft.push(item);
                }
                item.value = item.value + t.amount;

            }
            return transactionsDraft;
        };

        AppUtils.httpGet("transaction/transactions").then((response) => {
            var transactionsDraft = getStructuredData(response);
            this.setState({
                transactions: transactionsDraft
            })
        })
    }

    render() {

        const {transactions} = this.state;

        const radarChart = (
            <RadarChart cx={300} cy={250} outerRadius={150} width={600} height={500} data={transactions}>
                <Radar name="Spendings" dataKey="value" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6}/>
                <PolarGrid />
                <PolarAngleAxis dataKey="category"/>
                <PolarRadiusAxis/>
            </RadarChart>
        );

        const lineChart = (
            <LineChart width={600} height={400} data={transactions}
                       margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                <XAxis dataKey="category"/>
                <YAxis/>
                <CartesianGrid strokeDasharray="3 3"/>
                <Tooltip/>
                <Legend />
                <Line type="monotone" dataKey="value" stroke="#8884d8" activeDot={{r: 8}}/>
            </LineChart>
        );

        const pieChart = (
            <RadialBarChart width={730} height={250} innerRadius="10%" outerRadius="80%" data={transactions}>
                <RadialBar startAngle={90} endAngle={-270} minAngle={15} label background clockWise={true} dataKey='value' />
                <Legend iconSize={10} width={120} height={140} layout='vertical' verticalAlign='middle' align="right" />
                <Tooltip />
            </RadialBarChart>
        );

        return (
            <div>
                <Segment raised>
                    {radarChart}
                </Segment>
                <Segment raised>
                    {lineChart}
                </Segment>
                <Segment raised>
                    {pieChart}
                </Segment>
            </div>
        )
    }
}