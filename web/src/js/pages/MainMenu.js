import * as React from "react";
import {Menu} from "semantic-ui-react";


export default class MainMenu extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            activeItem: 'Home',
            onChange: this.props.onChange
        };
    }

    handleItemClick = (e, { name }) => {
        this.state.onChange(name);
        this.setState({ activeItem: name });
    }

    handleHomeClick = (e) => {
        this.handleItemClick(e, {name: 'Home'});
    }

    render() {
        const { activeItem } = this.state;

        return (
            <div className="mainMenu">
                <Menu pointing>
                    <Menu.Item name='Home' active={activeItem === 'Home'} onClick={this.handleHomeClick} />
                    <Menu.Item name='Statistics' active={activeItem === 'Statistics'} onClick={this.handleItemClick} />
                </Menu>
            </div>
        )
    }
}
