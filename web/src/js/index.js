import React from "react";
import ReactDOM from "react-dom";
import "../styles/App.scss";
import App from "./App";

//Error handling----------------------
window.onerror = (errorMsg, url, lineNumber, column, errorObj) => {
    alert(errorMsg);
};

ReactDOM.render(
    <App/>,
    document.getElementById('react-container')
);
