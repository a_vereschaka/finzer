'use strict';

let path = require('path');
let webpack = require('webpack');


let srcPath = '/../src';
let srcPathResolver = path.join(__dirname, srcPath);

module.exports.publicPath = '/';
module.exports.buildPath = '/../build';
module.exports.srcPath = srcPath;

let npmBase = path.join(__dirname, '/../node_modules');

module.exports.config = {
    entry: {
        vendor: [
            'react', 'react-dom'
        ]
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel-loader'],
                include: srcPathResolver
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.sass/,
                loader: 'style-loader!css-loader!sass-loader?outputStyle=expanded&indentedSyntax',
                include: srcPathResolver
            },
            {
                test: /\.scss/,
                loader: 'style-loader!css-loader!sass-loader?outputStyle=expanded'
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
                    'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            },
            {test: /\.(woff|woff2)$/, loader: "url-loader?limit=10000&mimetype=application/font-woff"},
            {test: /\.ttf$/, loader: "file-loader"},
            {test: /\.eot$/, loader: "file-loader"},
            {test: /\.ya?ml$/, loader: 'json-loader!yaml-loader'}]
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false
        }),
        new webpack.optimize.OccurrenceOrderPlugin()
    ],
    resolve: {
        extensions: ['.js', '.jsx'],
        alias: {
            npm: npmBase,
            app: srcPathResolver + '/js/',
            components: srcPathResolver + '/js/components/',
            containers: srcPathResolver + '/js/containers/',
            utils: srcPathResolver + '/js/utils/',
            styles: srcPathResolver + '/styles/',
            img: srcPathResolver + '/img/',
            i18n: srcPathResolver + '/js/i18n/'
        }
    }
};
