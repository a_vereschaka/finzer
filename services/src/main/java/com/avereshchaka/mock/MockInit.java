package com.avereshchaka.mock;

import com.avereshchaka.config.Constants;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 25 March 2017
 */
@Profile("!" + Constants.SPRING_PROFILE_PROD)
@Component
public class MockInit {

    private MockGenerator mockGenerator;

    public MockInit(MockGenerator mockGenerator) {
        this.mockGenerator = mockGenerator;
    }

    @PostConstruct
    public void runMock(){
//        mockGenerator.mockTransactions();
        //TODO right mocks for no dev mode
    }
}
