package com.avereshchaka.mock;

import com.avereshchaka.domain.Category;
import com.avereshchaka.domain.Currency;
import com.avereshchaka.domain.UserTransaction;
import com.avereshchaka.enums.TransactionType;
import com.avereshchaka.repository.CategoryRepository;
import com.avereshchaka.repository.CurrencyRepository;
import com.avereshchaka.repository.UserTransactionRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 25 March 2017
 */
@Component
@Transactional
public class MockGenerator {

    private UserTransactionRepository userTransactionRepository;
    private CategoryRepository categoryRepository;
    private CurrencyRepository currencyRepository;

    public MockGenerator(UserTransactionRepository userTransactionRepository, CategoryRepository categoryRepository, CurrencyRepository currencyRepository) {
        this.userTransactionRepository = userTransactionRepository;
        this.categoryRepository = categoryRepository;
        this.currencyRepository = currencyRepository;
    }

    public void mockTransactions() {
        Currency currency = new Currency();
        currency.setInterCode("UAH");
        currency.setName("Hryvnas");
        Category category = new Category();
        category.setName("Food");

        for (int i = 1; i < 11; i++) {

            UserTransaction ut = new UserTransaction();
            ut.setAmount(BigDecimal.valueOf((double)i / 4.0));
            ut.setCurrency(currency);
            ut.setDescription("Mocked transaction");
            ut.setType(i % 2 == 0 ? TransactionType.INCOME : TransactionType.OUTCOME);
            ut.setCategory(category);

            userTransactionRepository.save(ut);
        }
    }
}
