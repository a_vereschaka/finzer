package com.avereshchaka.service;

import com.avereshchaka.domain.Category;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 11 June 2017
 */
public interface CategoryService {
    Category getOrCreateForCurrentUser(String name);
}
