package com.avereshchaka.repository;

import com.avereshchaka.domain.UserTransaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 25 March 2017
 */
@Transactional
public interface UserTransactionRepository extends CrudRepository<UserTransaction, Long> {

    List<UserTransaction> findAllByUserIdEquals(String userId);

    Iterable<String> findDistinctCategoryByUserIdEquals(String userId);
}
