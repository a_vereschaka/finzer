package com.avereshchaka.repository;

import com.avereshchaka.domain.Currency;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 10 June 2017
 */
@Transactional
public interface CurrencyRepository extends CrudRepository<Currency, Long>{
}
