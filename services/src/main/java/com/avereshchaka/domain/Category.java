package com.avereshchaka.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 06 June 2017
 */
@Data
@Entity
public class Category {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String userId;
}
