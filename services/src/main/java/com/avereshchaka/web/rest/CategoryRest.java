package com.avereshchaka.web.rest;

import com.avereshchaka.config.Constants;
import com.avereshchaka.domain.UserTransaction;
import com.avereshchaka.repository.UserTransactionRepository;
import com.avereshchaka.utils.FilterUtils;
import com.avereshchaka.web.dto.CategoryDto;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 29 May 2017
 */
@RestController
@RequestMapping(path = Constants.BASE_API_URL + "/category")
public class CategoryRest {

    private UserTransactionRepository userTransactionRepository;

    public CategoryRest(UserTransactionRepository userTransactionRepository) {
        this.userTransactionRepository = userTransactionRepository;
    }

    @GetMapping("/categories")
    public Iterable<CategoryDto> getAllCategories(Principal principal){
        List<UserTransaction> categories = userTransactionRepository.findAllByUserIdEquals(principal.getName());
        List<CategoryDto> result = new ArrayList<>();

        if (CollectionUtils.isEmpty(categories)){
            return Collections.EMPTY_LIST;
        }

        categories.stream().filter(FilterUtils.distinctByKey(UserTransaction::getCategory)).forEach((userTransaction -> {
            CategoryDto categoryDto = new CategoryDto();
            categoryDto.setText(userTransaction.getCategory().getName());
            categoryDto.setValue(userTransaction.getCategory().getName());

            result.add(categoryDto);
        }));

        return result;
    }
}
