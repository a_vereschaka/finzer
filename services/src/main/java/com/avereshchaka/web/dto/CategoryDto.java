package com.avereshchaka.web.dto;

import lombok.Data;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 29 May 2017
 */
@Data
public class CategoryDto {

    private String text;
    private String value;
}
