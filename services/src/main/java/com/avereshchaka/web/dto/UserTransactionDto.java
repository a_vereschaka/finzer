package com.avereshchaka.web.dto;

import com.avereshchaka.enums.TransactionType;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 25 March 2017
 */
@Data
public class UserTransactionDto {

    private Long id;

    private BigDecimal amount;

    private String currency;

    private String category;

    private String description;

    private TransactionType type;

    private Long timeInMillis;

    public UserTransactionDto() {
    }
}
